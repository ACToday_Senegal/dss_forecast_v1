# -*- coding: utf-8 -*-
"""
Modified by Eunjin Han @IRI on Feb, 2020
======================================
# Redesigned on Thu December 26 17:28:23 2016
# @Re-designer: Seongkyu Lee, APEC Climate Center

##Program: CAMDT (Climate Agriculture Modeling Decision Tool)
##  The CAMDT is a computer desktop tool designed to guide decision-makers
##  in adopting appropriate crop and water management practices
##  that can improve crop yields given a climate condition
##Author: Eunjin Han
##Institute: IRI-Columbia University, NY
##Revised: August, 2, 2016
##Date: February 17, 2016
##
##Redesigned: December 26, 2016 (by Seongkyu Lee, APEC Climate Center)
##
##===================================================================
"""

from SIMAGRI._configuration import *

class UIVarDSSATSetup2():

  _Section_Name = 'DSSATSetup2'

  # Fertilization application
  rbFertApp = None # Fbutton1
  
  # FA_nfertilizer = None # nfertilizer
  FA_day1 = None 
  FA_fert_mat1 = None 
  FA_fert_app_method1 = None 
  FA_amt_depth1 = None 
  FA_amt_N1 = None 
  FA_amt_P1 = None 
  FA_amt_K1 = None 
  FA_day2 = None 
  FA_fert_mat2 = None 
  FA_fert_app_method2 = None 
  FA_amt_depth2 = None 
  FA_amt_N2 = None 
  FA_amt_P2 = None 
  FA_amt_K2 = None 
  FA_day3 = None 
  FA_fert_mat3 = None 
  FA_fert_app_method3 = None 
  FA_amt_depth3 = None 
  FA_amt_N3 = None 
  FA_amt_P3 = None 
  FA_amt_K3 = None 
  FA_day4 = None 
  FA_fert_mat4 = None 
  FA_fert_app_method4 = None 
  FA_amt_depth4 = None 
  FA_amt_N4 = None 
  FA_amt_P4 = None 
  FA_amt_K4 = None 
  FA_day5 = None 
  FA_fert_mat5 = None 
  FA_fert_app_method5 = None 
  FA_amt_depth5 = None 
  FA_amt_N5 = None 
  FA_amt_P5 = None 
  FA_amt_K5 = None 

  # Irrigation
  rbIrrigation = None # IRbutton
  IR_method = None  #irrigation method, entryfield
  IA_mng_depth = None  
  IA_threshold = None 
  IA_eff_fraction = None 
  ir_m1 = None  
  ir_d1 = None  
  ir_y1 = None 
  ir_amt1 = None 
  ir_m2 = None 
  ir_d2 = None 
  ir_y2 = None 
  ir_amt2 = None 
  ir_m3 = None  
  ir_d3 = None  
  ir_y3 = None 
  ir_amt3 = None 
  ir_m5 = None 
  ir_d5 = None 
  ir_y5 = None 
  ir_amt5 = None 

  def __init__(self):
    pass

  def initVars(self):
    pass

  # load from configuration file
  def loadConfig(self):
    # Fertilization application
    set_config_value_from_pmw_entryfield(self._Section_Name, 'rbFertApp', self.rbFertApp)  # Fbutton1

    # get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_nfertilizer', self.FA_nfertilizer) # nfertilizer

    # 1st application
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_day1', self.FA_day1)  # day1
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_mat1', self.FA_fert_matl1) # fert_mat1
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_app_method1', self.FA_fert_app_method1) # fert_app1
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_depth1', self.FA_amt_depth1)  # depth
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_N1', self.FA_amt_N1)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_P1', self.FA_amt_P1) 
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_K1', self.FA_amt_K1)  
    # 2nd application
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_day2', self.FA_day2)  # day1
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_mat2', self.FA_fert_matl2) # fert_mat1
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_app_method2', self.FA_fert_app_method2) # fert_app1
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_depth2', self.FA_amt_depth2)  # depth
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_N2', self.FA_amt_N2)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_P2', self.FA_amt_P2) 
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_K2', self.FA_amt_K2)  
    # 3rd application
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_day3', self.FA_day3)  
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_mat3', self.FA_fert_matl3) # fert_mat1
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_app_method3', self.FA_fert_app_method3) # fert_app1
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_depth3', self.FA_amt_depth3)  # depth
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_N3', self.FA_amt_N3)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_P3', self.FA_amt_P3) 
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_K3', self.FA_amt_K3)  
    #4th application
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_day4', self.FA_day4) 
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_mat4', self.FA_fert_matl4) # fert_mat1
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_app_method4', self.FA_fert_app_method4) # fert_app1
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_depth4', self.FA_amt_depth4)  # depth
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_N4', self.FA_amt_N4)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_P4', self.FA_amt_P4) 
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_K4', self.FA_amt_K4)  
    #5th application
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_day5', self.FA_day5)  
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_mat5', self.FA_fert_mat5) # fert_mat1
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_app_method5', self.FA_fert_app_method5) # fert_app1
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_depth5', self.FA_amt_depth5)  # depth
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_N5', self.FA_amt_N5)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_P5', self.FA_amt_P5) 
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amt_K5', self.FA_amt_K5)  

    # Irrigation
    get_config_value_into_tkinter_intvar(self._Section_Name, 'rbIrrigation', self.rbIrrigation)  # IRbutton
    get_config_value_into_pmw_combobox(self._Section_Name, 'IR_method', self.IR_method)  # automatic irrigation method
    get_config_value_into_pmw_entryfield(self._Section_Name, 'IA_mng_depth', self.IA_mng_depth)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'IA_threshold', self.IA_threshold)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'IA_eff_fraction', self.IA_eff_fraction)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_m1', self.ir_m1)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_d1', self.ir_d1)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_y1', self.ir_y1)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_amt1', self.ir_amt1)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_m2', self.ir_m2)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_d2', self.ir_d2)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_y2', self.ir_y2)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_amt2', self.ir_amt2)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_m3', self.ir_m3)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_d3', self.ir_d3)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_y3', self.ir_y3)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_amt3', self.ir_amt3) 
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_m4', self.ir_m4)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_d4', self.ir_d4)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_y4', self.ir_y4)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_amt4', self.ir_amt4)   
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_m5', self.ir_m5)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_d5', self.ir_d5)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_y5', self.ir_y5)  
    get_config_value_into_pmw_entryfield(self._Section_Name, 'ir_amt5', self.ir_amt5)  

  # save into configration file
  def saveConfig(self):
    # Fertilization application
    set_config_value_from_tkinter_intvar(self._Section_Name, 'rbFertApp', self.rbFertApp)  # Fbutton1

    # set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_nfertilizer', self.FA_nfertilizer)  # nfertilizer
    # 1st application
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_day1', self.FA_day1)  # day1
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_mat1', self.FA_fert_mat1) # fert_mat1
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_app_method1', self.FA_fert_app_method1) # fert_app1
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_depth1', self.FA_amt_depth1)  # depth
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_N1', self.FA_amt_N1)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_P1', self.FA_amt_P1) 
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_K1', self.FA_amt_K1)  
    # 2nd application
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_day2', self.FA_day2)  # day1
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_mat2', self.FA_fert_mat2) # fert_mat1
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_app_method2', self.FA_fert_app_method2) # fert_app1
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_depth2', self.FA_amt_depth2)  # depth
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_N2', self.FA_amt_N2)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_P2', self.FA_amt_P2) 
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_K2', self.FA_amt_K2)  
    # 3rd application
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_day3', self.FA_day3)  
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_mat3', self.FA_fert_mat3) # fert_mat1
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_app_method3', self.FA_fert_app_method3) # fert_app1
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_depth3', self.FA_amt_depth3)  # depth
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_N3', self.FA_amt_N3)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_P3', self.FA_amt_P3) 
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_K3', self.FA_amt_K3)  
    #4th application
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_day4', self.FA_day4) 
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_mat4', self.FA_fert_mat4) # fert_mat1
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_app_method4', self.FA_fert_app_method4) # fert_app1
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_depth4', self.FA_amt_depth4)  # depth
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_N4', self.FA_amt_N4)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_P4', self.FA_amt_P4) 
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_K4', self.FA_amt_K4)  
    #5th application
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_day5', self.FA_day5)  
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_mat5', self.FA_fert_mat5) # fert_mat1
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_app_method5', self.FA_fert_app_method5) # fert_app1
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_depth5', self.FA_amt_depth5)  # depth
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_N5', self.FA_amt_N5)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_P5', self.FA_amt_P5) 
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amt_K5', self.FA_amt_K5)  
    
    # Irrigation
    set_config_value_from_tkinter_intvar(self._Section_Name, 'rbIrrigation', self.rbIrrigation)  # IRbutton
    set_config_value_from_pmw_combobox(self._Section_Name, 'IR_method', self.IR_method)  # automatic irrigation method
    set_config_value_from_pmw_entryfield(self._Section_Name, 'IA_mng_depth', self.IA_mng_depth)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'IA_threshold', self.IA_threshold)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'IA_eff_fraction', self.IA_eff_fraction)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_m1', self.ir_m1)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_d1', self.ir_d1)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_y1', self.ir_y1)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_amt1', self.ir_amt1)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_m2', self.ir_m2)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_d2', self.ir_d2)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_y2', self.ir_y2)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_amt2', self.ir_amt2)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_m3', self.ir_m3)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_d3', self.ir_d3)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_y3', self.ir_y3)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_amt3', self.ir_amt3) 
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_m4', self.ir_m4)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_d4', self.ir_d4)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_y4', self.ir_y4)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_amt4', self.ir_amt4)   
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_m5', self.ir_m5)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_d5', self.ir_d5)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_y5', self.ir_y5)  
    set_config_value_from_pmw_entryfield(self._Section_Name, 'ir_amt5', self.ir_amt5)  
